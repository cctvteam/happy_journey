<?php
use think\Db;
//积分日志
 function jifenlog($orderNum,$userid,$cash = '0.00',$type = '0',$status = '0',$beizhu='',$gamename='')
 {
 
     $data ['orderid'] = $orderNum;
     $data ['userid'] = $userid;//会员id
     $data ['jifen'] = $cash; //变动积分
     $data ['type'] = $type;  //type=1 增加 0减少 2充值
     $data ['status'] = $status; //1成功 0失败
     $data ['beizhu'] =$beizhu;
     $data ['gamename'] =$gamename;
     
     $flag = Db::name('jifen_log')->insert( $data );
     return $flag;
     
 }

 function getUser($userid=null)
 {
 	if(!$userid){
 		echo 'need login'; die();
 	}
 	
 	$userdet = Db::name('User')->field('id,name,jifen,phone,runaddress')->find($userid);
 	
 	return $userdet;
 }	