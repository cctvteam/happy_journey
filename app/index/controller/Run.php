<?php
namespace app\index\controller;
use think\Request;
use think\Db;
use think\Controller;
/*
*接口控制器
*/
class  Run extends Base
{
    public function index(){
         $user = Db::name('user')->where('id',session('userid'))->find();
         $phone  =substr($user['phone'],0,-8)."****".substr($user['phone'],-4);
         $this->assign('phone',$phone);
		 $this->assign('user',$user);
         return $this->fetch();
    }
    public function runlist(){
         return $this->fetch();
    }
    public function addlist(){
         return $this->fetch();
    }
    public function addsave(){
            if($this->request->isPost()) {
                $data = $this->request->post();
                $det = Db::name('user')->where('id',session('userid'))->find();
                $pwd = md5($data['jioayi'].'lfcy');
                if($det['jiaoyi'] != $pwd){
                    return $this->error('交易密码不正确');
                }else{
                    return $this->success('添加成功','index/index/index');
                }
            }
    }
    public function checkpass(){
        $res =Db::name('user')->field('jiaoyi')->where('id',session('userid'))->find();
        $jiaoyi =$res['jiaoyi'];
		if(empty($jiaoyi)){
			 $result['code'] = 1;
			 return json($result);exit;
		}else{
             $result['code'] = 2;
			 return json($result);exit;
        }
    }
}
